package ee.bcs.valiit;

//this dog is implementation
public class Dog extends LivingThing {

    public String getName() {
        return name;
    }

    //Kommentaar 1. Mis ma oskan öelda?

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public void bark() {
        System.out.println(String.format("%s on %s ja tegi Auh-Auh! %s", name, getType(), Main.BIG_CONSTANT));
    }
}

